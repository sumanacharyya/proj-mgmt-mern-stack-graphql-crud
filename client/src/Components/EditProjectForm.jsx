import { useMutation } from "@apollo/client";
import React, { useState } from "react";
import { GET_PROJECT } from "../queries/projectQueries";
import { UPDATE_PROJECT } from "../mutations/projectMutations";

const statusSetterVal = {
  "New project": "new",
  "In Progress": "progress",
  Completed: "completed",
};

const EditProjectForm = ({ project }) => {
  const [fields, setFields] = useState({
    name: project.name,
    description: project.description,
    status: statusSetterVal[project.status],
  });

  const [updateProject] = useMutation(UPDATE_PROJECT, {
    variables: {
      id: project.id,
      name: fields.name,
      description: fields.description,
      status: fields.status,
    },
    refetchQueries: [{ query: GET_PROJECT, variables: { id: project.id } }],
  });

  const onSubmit = (e) => {
    e.preventDefault();

    if ((!fields.name, !fields.description, !fields.status)) {
      return alert("Please fill up all the fields...!");
    }
    updateProject(fields.name, fields.description, fields.status);
  };

  return (
    <div className="mt-5 ">
      <h3>Update Project Details</h3>
      <form onSubmit={onSubmit}>
        <div className="mb-3">
          <label className="form-label">Name</label>
          <input
            type="text"
            className="form-control"
            id="name"
            value={fields.name}
            onChange={(e) => setFields({ ...fields, name: e.target.value })}
          />
        </div>
        <div className="mb-3">
          <label className="form-label">Description</label>
          <textarea
            className="form-control"
            id="description"
            value={fields.description}
            onChange={(e) =>
              setFields({
                ...fields,
                description: e.target.value,
              })
            }
          />
        </div>
        <div className="mb-3">
          <label className="form-label">Status</label>
          <select
            type="text"
            id="status"
            className="form-select"
            value={fields.status}
            onChange={(e) => setFields({ ...fields, status: e.target.value })}
          >
            <option value="new">Not started</option>
            <option value="progress">In progress</option>
            <option value="completed">Completed</option>
          </select>
        </div>

        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      </form>
    </div>
  );
};

export default EditProjectForm;
