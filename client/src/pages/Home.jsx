import React from "react";
import AddClientModal from "../Components/AddClientModal";
import Projects from "../Components/Projects";
import Clients from "../Components/Clients";
import AddProjectModal from "../Components/AddProjectModal";

const Home = () => {
  return (
    <>
      <div className="d-flex gap-3 mb-4">
        <AddClientModal />
        <AddProjectModal />
      </div>
      <hr />
      <hr />

      <Projects />
      <hr />
      <Clients />
    </>
  );
};

export default Home;
