require("dotenv").config();
const express = require("express");
const { graphqlHTTP } = require("express-graphql");
const colors = require("colors");
const cors = require("cors");
const morgan = require("morgan");

const connectDb = require("./config/db");
const schemaValues = require("./schema/schema");

const PORT = process.env.PORT || 8080;
const app = express();

connectDb();

app.use(cors());
app.use(morgan("dev"));

app.use(
  "/graphql",
  graphqlHTTP({
    schema: schemaValues,
    graphiql: process.env.NODE_ENV === "developement",
  })
);

app.listen(PORT, () => {
  console.log(`Server listening at port --> http://localhost:${PORT}`);
  console.log(
    `Server listening for testing at port --> http://localhost:${PORT}/graphql`
  );
});
