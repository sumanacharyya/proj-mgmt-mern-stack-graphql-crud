const e = require("express");
const mongoose = require("mongoose");

mongoose.set("strictQuery", true);

const connectDb = async () => {
  const connect = await mongoose.connect(process.env.MONGO_URI);
  console.log(
    `MongoDB Connected: ${connect.connection.host}!`.blue.underline.bold
  );
};

module.exports = connectDb;
